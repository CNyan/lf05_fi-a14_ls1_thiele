import java.util.Scanner;

public class Name {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte Ihren Namen ein: ");
		String name = myScanner.next();
		
		System.out.println("Verraten Sie mir bitte Ihr Alter:");
		int alter = myScanner.nextInt();
		
		System.out.println("Ihr Name ist: "+ name + "! Ihr Alter ist: "+ alter);
	}

}
