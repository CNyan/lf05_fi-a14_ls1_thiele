
public class Konfigurationstest {

	public static void main(String[] args) {
		//Variablen
		//�bung 1
		//a
		int cent;
		cent = 70;
		cent = 80;
		//b
		double maximum;
		maximum = 95.50;
		//�bung 2
		// : true | -1000 | 1,255 | #
		boolean a = true ;
		short b = -1000 ;
		float c = 1.255f ;
		char d = '#';
		//�bung 3
		//a
		String wort = "Ich bin Satz" ;
		//b
		final int check_nr = 8765;
		//�bung 4
		/* Datentypen sind in Programmiersprachen wichtig um Daten effizient zu speichern 
		 * und Speicherbereichen eine gewisse Semandtik zuzuweisen
		 */
		
		
		//Operatoren
		//�bung 1
		//a
		int ergebnis;
		ergebnis = 4 + 8 * 9 - 1;
		System.out.println(ergebnis);
		//b
		int zaehler = 1;
		zaehler += 1;
		System.out.println(zaehler);
		//c
		int ganzzahldivi = 22/6;
		System.out.println(ganzzahldivi);
		
		//�bung 2:
		
		int schalter = 10;
		//a
		System.out.println(schalter > 7 && schalter < 12);
		//b
		System.out.println(schalter != 10 || schalter == 12);
		
		//�bung 3
		String ab , bb , cb;
		ab = "Meine Oma ";
		bb = "f�hrt im " ;
		cb = "H�hnerstall Motorrad.";
		System.out.println(ab + bb + cb);
	}

}
