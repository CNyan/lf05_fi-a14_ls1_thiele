
public class �bung1 {

	public static void main(String[] args) {
		//Vorgegebener Satz
		System.out.println("Das ist ein Beispielsatz. Ein Beispielsatz ist das.\n");
		
		// Verschiedene Testungen
		System.out.println("Das ist ein \"Beispielsatz\".\nEin Beispielsatz ist das.\r");
		System.out.print("Das ist ein Beispielsatz.\r" + "Ein \'Beispielsatz\' ist das.\n");
		
		//Mit Variable
		String beispiel = "\"Beispielsatz\"";
		System.out.println("Das ist ein " + beispiel + "\nEin " + beispiel+ "ist das.\n");
		
		/* Der Unterschied zwischen print und println ist, dass f�r print ein extra Zeilenumbruch mit \n oder \r 
		 * eingef�gt werden muss um die Zeil zu brechen, w�hrend println einen Zeilenumbruch nach der komplette Anweisung 
		 * einf�gt
		 */
				
		System.out.printf( "%7s\n", "*" );
		System.out.printf( "%8s\n", "***" );
		System.out.printf( "%9s\n", "*****" );
		System.out.printf( "%10s\n", "*******" );
		System.out.printf( "%11s\n", "*********" );
		System.out.printf( "%12s\n", "***********" );
		System.out.printf( "%8s\n", "***" );
		System.out.printf( "%8s\n", "***" );
		System.out.printf( "%8s\n", "***" );
		System.out.println();
		
		System.out.printf( "%.2f\n" ,22.4234234);
		System.out.printf( "%.2f\n" ,111.2222);
		System.out.printf( "%.2f\n" ,4.0);
		System.out.printf( "%.2f\n" ,1000000.551);
		System.out.printf( "%.2f\n" ,97.34);
	    	          
	}
 
}
